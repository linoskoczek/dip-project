import cv2
import numpy as np
from matplotlib import pyplot as plt

# load img
img = cv2.imread('img/alpaca.jpg', 0)

# show histogram of img
plt.hist(img.ravel(), 256, [0, 256])
plt.savefig('img/histogram-alpaca.png')
plt.clf()
plt.cla()

# equalization output
equ = cv2.equalizeHist(img)
cv2.imwrite('img/alpaca-equalized.jpg', equ)

# show histogram of pic after equalization
img2 = cv2.imread('img/alpaca-equalized.jpg', 0)
plt.hist(img2.ravel(), 256, [0, 256])
plt.savefig('img/histogram-alpaca-equalized.png')
plt.clf()
plt.cla()

# show histograms of dark, light, lowcontrast, highcontrast
files = ('alpaca-dark', 'alpaca-light', 'alpaca-lowcontrast', 'alpaca-highcontrast')


for f in files:
    input_name = f + '.jpg'
    output_name = f + '-equalized' + '.jpg'

    # save histograms of images
    img2 = cv2.imread('img/' + f + '.jpg', 0)
    plt.hist(img2.ravel(), 256, [0, 256])
    plt.savefig('img/histogram-' + f + '.png')
    plt.clf()
    plt.cla()

    # save equalized images
    img = cv2.imread('img/' + input_name, 0)
    equ = cv2.equalizeHist(img)
    cv2.imwrite('img/' + output_name, equ)

    img2 = cv2.imread('img/' + f + '-equalized.jpg', 0)
    plt.hist(img2.ravel(), 256, [0, 256])
    plt.savefig('img/histogram-' + f + '-equalized.png')
    plt.clf()
    plt.cla()