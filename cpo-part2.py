import cv2
import numpy as np
import scipy.misc
import scipy.ndimage
from matplotlib import pyplot as plt
from matplotlib import cm

# BLUR
# read image
src = cv2.imread('img2/moon.png', cv2.IMREAD_UNCHANGED)

# apply guassian blur on src image
dst = cv2.GaussianBlur(src, (3, 3), cv2.BORDER_DEFAULT)
cv2.imwrite('img2/moon-blur.jpg', dst)

## LAPLACIAN SHARPENING
# function for plotting abs:
pic_n = 1


def show_abs(I, plot_title):
    plt.title(plot_title)
    plt.tight_layout()
    plt.axis('off')
    plt.imshow(abs(I), cm.gray)


# reading of the image into numpy array
A0 = scipy.misc.imread('img2/moon-blur.jpg', flatten=True)
A0 -= np.amin(A0)  # map values to the (0, 255) range
A0 *= 255.0/np.amax(A0)

# kernel for negative Laplacian
kernel = np.ones((3, 3))*(-1)
kernel[1, 1] = 8

# convolution of the image with the kernel
Lap = scipy.ndimage.filters.convolve(A0, kernel)

# map laplacian to some new range
Laps = Lap*100.0/np.amax(Lap)  # sharpening factor

plt.figure(pic_n)
pic_n += 1
plt.subplot(1, 2, 1)
show_abs(Lap, 'Laplacian')
plt.subplot(1, 2, 2)
show_abs(Laps, 'Scaled Laplacian')

A = A0 + Laps  # Add negative Laplacian to the original image

A = abs(A)  # Get rid of negative values
A *= 255.0/np.amax(A)

# Local Histogram Equalization with OpenCV:
A_cv2 = A
A_cv2 = A_cv2.astype(np.uint8)

tile_s0 = 4
tile_s1 = 4

clahe = cv2.createCLAHE(clipLimit=1, tileGridSize=(tile_s0, tile_s1))
A_cv2 = clahe.apply(A_cv2)

plt.figure(pic_n)
pic_n += 1
plt.subplot(1, 3, 1)
show_abs(A0, 'Original image')
plt.subplot(1, 3, 2)
show_abs(A, 'Laplacian filtered img')
plt.subplot(1, 3, 3)
show_abs(A_cv2, 'Local Hist equalized img')
plt.show()

## unsharp

unsharp_image = cv2.addWeighted(src, 1.5, dst, -0.5, 0, src)
cv2.imwrite('img2/moon-unsharp.jpg', unsharp_image)